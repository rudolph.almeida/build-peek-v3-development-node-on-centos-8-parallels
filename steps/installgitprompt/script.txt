curl -L "https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh" \
    -o "${HOME}/.git-prompt.sh"

chmod +x ${HOME}/.git-prompt.sh
