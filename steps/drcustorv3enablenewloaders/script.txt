
psql <<EOF

UPDATE pl_enmac_event_loader."AppServerSettings"
SET enabled = true;

UPDATE pl_enmac_livedb_loader."AppServerSettingsTuple"
SET enabled = true;

UPDATE pl_enmac_switching_loader."AppServerSettings"
SET enabled = true;

UPDATE pl_enmac_event_loader."AppServerSettings"
SET enabled = true;


EOF
