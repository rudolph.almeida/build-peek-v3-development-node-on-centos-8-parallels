echo  "Deleting VM : '{targetServer.hostname}'"
if prlctl list {targetServer.hostname} ;
then
    echo "VM Found"
    
    if prlctl delete {targetServer.hostname} ;
    then  
        echo "Success"
    else 
        prlctl stop {targetServer.hostname} --kill
        prlctl delete {targetServer.hostname}
    fi
else
    echo "No VM, continuing."
fi
