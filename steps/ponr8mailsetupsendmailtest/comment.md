Test that the sendmail service is working and will send mail to the right email address.

TROUBLESHOOTING
The sendmail log file is normally at /var/log/maillog, but this can be changed depending on the /etc/syslog.conf file.
This command "grep -F 'mail.*' /etc/syslog.conf" will show you where the log file is.