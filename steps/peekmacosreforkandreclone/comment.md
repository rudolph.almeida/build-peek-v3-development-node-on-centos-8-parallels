Handles setup of a basic Synerty DevOps environment on macOS.
Intended to be run at some point after the DevOps App VM has been set up.